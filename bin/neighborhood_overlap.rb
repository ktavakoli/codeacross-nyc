require 'rubygems'
require 'json'
require 'mongo'
require 'date'

conn = Mongo::MongoClient.new("127.0.0.1", "27017", :op_timeout => 2400)
db = conn["taxiData"]
hoods = db.collection("nycNeighborhoods")
boroughs = db.collection("nycBoroughs")

total = hoods.count
total_overlap = 0
have_overlap = 0

hoods.find.each_with_index do |h, i|
	query = {"_id" => {"$ne" => h["_id"]}, "properties.neighborhood" => {"$ne" => h["properties"]["neighborhood"]}, "geometry" => { "$geoIntersects" => {"$geometry" => h["geometry"] } } }
	matches = hoods.find(query).count()
	puts "#{matches} found for #{h['properties']['neighborhood']} (#{i+1} of #{total})"
	if matches > 0
		hoods.find(query).each { |m| puts "***** Overlap found between #{m['properties']['neighborhood']} and #{h['properties']['neighborhood']}." }
		total_overlap += matches
		have_overlap += 1
	end
end

puts "The neighborhoods with overlap: #{have_overlap} of #{total}"
puts "The total overlap found between these neighborhoods: #{total_overlap}"

conn.close