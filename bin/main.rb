require 'rubygems'
require 'json'
require 'mongo'
require 'date'
require_relative 'setup_aggregation.rb'
require_relative 'map_reduce.rb'

start_time = Time.now
puts "#{"*" * 10} Execution started at #{start_time}. #{"*" * 10}"

conn = Mongo::MongoClient.new("127.0.0.1", "27017", :op_timeout => 2400)
db = conn["taxiData"]
types = ["dropoff", "pickup"]

limit = 100000

source_trips = 		db.collection("taxiTrips")
sample_trips = 		db.collection("temp_taxiTrips")
final_trips = 		db.collection("final_taxiTrips")
hoods = 					db.collection("temp_nycNeighborhoods")
boroughs = 				db.collection("temp_nycBoroughs")
unique_boroughs = db.collection("uniqueBoroughs")
unique_hoods = 		db.collection("uniqueHoods")
stats = 					db.collection("summaryStats")
facts = 					db.collection("factCheck")

total_records = source_trips.count 
total_chunks = total_records/limit
puts "#{"*" * 10} #{total_records} records (or #{total_chunks+1} chunks of #{limit} records each) found in the 'taxiTrips' table. #{"*" * 10}"

db.initialize_db 
stats.drop
facts.drop
final_trips.drop

# sample_db.clean_db <= This call should not be necessary 
# if I just re-import all of the data from scratch again.
# This was only a problem because previous versions of the 
# script modified the base collections, rather than copying
# them and doing the calculations on the collection copies.

boroughs.create_geo_index
hoods.create_geo_index
hoods.set_hood_codes

boroughs.find_unique_boroughs(unique_boroughs)
sample_trips.create_trip_indexes(types)

(0..total_chunks).each do |n|
	current_time = Time.now
	duration = current_time-start_time
	puts "#{"*" * 10} Evaluating the first #{(n+1)*limit} records... (Total time: #{time_converter(duration)}) #{"*" * 10}"

	source_trips.copy_sample(sample_trips, limit, n)
	sample_trips.update_dates_and_locations

	types.each { |t| sample_trips.find_trip_boroughs(boroughs, t) }
  types.each { |t| sample_trips.update_hoods(unique_boroughs, hoods, t) }
  sample_trips.generate_summary_stats(stats, types)
  sample_trips.copy_to_final(final_trips)

	est_time = (Time.now-start_time)/(n+1) * (total_chunks+1)
	puts "#{"*" * 10} First #{(n+1)*limit} records evaluated. Total time estimate: #{time_converter(est_time)} #{"*" * 10}"
end

stats.perform_fact_check(facts)
stats.flatten_summary

conn.close
end_time = Time.now
duration = end_time-start_time
puts "#{"*" * 10} Execution ended at #{end_time} (Total time: #{time_converter(duration)}). #{"*" * 10}"