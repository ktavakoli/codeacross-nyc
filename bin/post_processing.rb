require 'rubygems'
require 'json'
require 'mongo'
require 'date'
require_relative 'setup_aggregation.rb'
require_relative 'map_reduce.rb'

conn = Mongo::MongoClient.new("127.0.0.1", "27017", :op_timeout => 2400)
db = conn["taxiData"]
hoods = db.collection("nycNeighborhoods")
unique_hoods = db.collection("uniqueHoods")

hoods.find_unique_hoods(unique_hoods)
unique_hoods.create_geo_index
