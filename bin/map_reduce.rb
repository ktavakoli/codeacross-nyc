def map(type)
	map_function = <<-EOF
		function() {
			key = { 
				data: this['#{type}_data'],
	      type: '#{type}'
	    };
	    value = {count: 1};
	    emit(key, value);
		}
	EOF
	return map_function
end

def reduce
	reduce_function = <<-EOF
		function(key, values) {
	    res = values[0];
	    for (var i = 1; i<values.length; i++) {
	        res.count = res.count + values[i]['count'];
	    }
	    return res;
		}
	EOF
	return reduce_function
end

def summary_map(keys)
	map_function = <<-EOF
		function() {
		    key = {
		        type: this.type,
  EOF
  keys.each { |k| map_function += " #{k}: this.#{k}, " }
  map_function += <<-EOF
		    };
		    value = {count: this.count};
		    emit(key, value);
		}
	EOF
end

def trip_map(keys)
	map_function = <<-EOF
		function() {
		    key = {
		    	dropoff_data : {
		    		neighborhood_code : this.dropoff_data.neighborhood_code,
		    		neighborhood : this.dropoff_data.neighborhood,
		    		borough : this.dropoff_data.borough,
		    		borough_code : this.dropoff_data.borough_code
	    		},
		    	pickup_data : {
		    		neighborhood_code : this.pickup_data.neighborhood_code,
		    		neighborhood : this.pickup_data.neighborhood,
		    		borough : this.pickup_data.borough,
		    		borough_code : this.pickup_data.borough_code
	    		},
  EOF
  keys.each { |k| map_function += " #{k}: this.#{k}, " }
  map_function += <<-EOF
		    };
		    value = {count: 1};
		    emit(key, value);
		}
	EOF
end

def fact_check_map
	map_function = <<-EOF
		function () {
			key = {type: this['_id']['type']};
			value = {count: this['value']['count']};
	    emit(key, value);
		}
	EOF
	return map_function
end

def reduce_locations
	reduce_function = <<-EOF
		function(key, values) {
    	return values;
		}
	EOF
	return reduce_function
end	

def map_locations
	map_function = <<-EOF
		function() {
		    Date.prototype.getWeek = function() {
		        var onejan = new Date(this.getFullYear(),0,1);
		        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
		    }
		    
		    String.prototype.getLZ = function () {
		        x = this;
		        while (x.length < 2) x = '0' + x;
		        return x;
		    }
		    
		    Date.prototype.getMinRange = function() {
		        x1 = this.getMinutes();
		        x1 = x1 / 15;
		        x2 = ((Math.floor(x1) + 1) * 15).toString().getLZ();
		        x1 = (Math.floor(x1)*15).toString().getLZ();
		        return x1 + '-' + x2;
		    }
		    
		    Date.prototype.getStats = function () {
		        dateStats = {};
		        var weekday = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		        dateStats['day_id'] = this.getDay();
		        dateStats['day'] = weekday[dateStats['day_id']];
		        dateStats['date'] = this;
		        dateStats['year'] = this.getFullYear();
		        dateStats['week_num'] = this.getWeek();
		        dateStats['hour'] = this.getHours();
		        dateStats['min_range'] = this.getMinRange();
		        dateStats['time_range'] = this.getHours().toString().getLZ() + '-'+ this.getMinRange();        
		        return dateStats;
		    }
		    var weekday = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

		    var dropoff_date = new Date(this.Lpep_dropoff_datetime);
		    var pickup_date = new Date(this.lpep_pickup_datetime);
		    var p_day_id = pickup_date.getDay();
		    var p_day = weekday[p_day_id];
		    key = this;
		    value = {
		        'dropoff_loc' : {
		            'type' : 'Point',
		            'coordinates' : [this.Dropoff_longitude,this.Dropoff_latitude]
		            },
		        'pickup_loc' : { 
		            'type' : 'Point',
		            'coordinates' : [this.Pickup_longitude,this.Pickup_latitude]
		            },
		        'dropoff_data' : dropoff_date.getStats(),
		        'pickup_data' : pickup_date.getStats()
		    };
		    emit(key, value)
		}
		EOF
	return map_function
end	