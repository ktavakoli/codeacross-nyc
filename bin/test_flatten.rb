require 'rubygems'
require 'json'
require 'mongo'
require 'date'
require_relative 'setup_aggregation.rb'
require_relative 'map_reduce.rb'

conn = Mongo::MongoClient.new("127.0.0.1", "27017", :op_timeout => 2400)
db = conn["taxiData"]

stats = db.collection("summaryStats")
stats.flatten_summary
