def time_converter(x)
	sec_in_min = 60
	sec_in_hour = sec_in_min * 60
	sec_in_day = sec_in_hour * 24
	if x < sec_in_min
		x = "#{x.round(1)} seconds"
	elsif x > sec_in_min && x < sec_in_hour
		x = "#{(x*1.0/sec_in_min).round(1)} minutes"
	elsif x > sec_in_hour && x < sec_in_day
		x = "#{(x*1.0/sec_in_hour).round(1)} hours"
	elsif x > sec_in_day
		x = "#{(x*1.0/sec_in_day).round(1)} days"
	end
	return x
end

class Mongo::DB
	def initialize_db
		puts "[#{__method__}] #{self.collections.count} collections in the existing database."

		sample_collections = [
			{coll: "nycNeighborhoods", limit: nil},
			{coll: "nycBoroughs", limit: nil}
		]

		sample_collections.each do |s|
			coll = self.collection(s[:coll])
			pipeline = [{"$out" => "temp_#{coll.name}" }]
			coll.aggregate(pipeline)
			puts "[#{__method__}] Copied collection '#{coll.name}' successfully..."
		end

		puts "[#{__method__}] Temp collections successfully copied from the original collections."
	end

	def clean_db
		collections = [
			{coll: "temp_taxiTrips", remove_fields: ["dropoff_borough", "start_hood"]},
			{coll: "temp_nycNeighborhoods", remove_fields: ["evaluated", "sample_matches"]},
			{coll: "temp_nycBoroughs", remove_fields: ["evaluated", "sample_matches"]}
		]

		collections.each do |c|
			coll = collection(c[:coll])
			bulk = coll.initialize_unordered_bulk_op
			subtotal = 0
			c[:remove_fields].each do |f|
				query = {f => {"$exists" => true}}
				subtotal = coll.find(query).count()
				if subtotal > 0
					bulk.find(query).update({"$unset" => {f => true }})
				end
				puts "[#{__method__}] #{f} removed from #{c[:coll]}."
			end
			bulk.execute if subtotal > 0
		end

		puts "[#{__method__}] Database successfully cleaned up."
	end

end

class Mongo::Collection
	def set_hood_codes
		puts "[#{__method__}] Setting neighborhood codes..."		
		unique_hoods = self.find.map { |h| h["properties"]["neighborhood"] }.uniq!
		unique_hoods.each_with_index do |h, hi|
			find_query = { "properties.neighborhood" => h }
			update_params = { "$set" => { "properties.neighborhood_code" => hi+1 }}
			self.update(find_query,update_params, {multi: true})
		end
		puts "[#{__method__}] Neighborhood codes set."		
	end

	def find_unique_hoods(unique_hoods)
		unique_hoods.drop
		x = self.find.map { |h| {
			neighborhood: h["properties"]["neighborhood"]} }.uniq!
		puts	x.class
		x.each_with_index do |h, hi|
			docs = self.find({"properties.neighborhood" => h[:neighborhood]})
			puts "#{docs.count} records found for #{h[:neighborhood]}."

			fd = docs.to_a[0]

			new_doc = {
				"type" => fd["type"],
				"properties" => fd["properties"],
				"geometry" => {}
			}
			new_doc["properties"]["neighborhood_code"] = hi+1
			new_doc["properties"]["polygons_found"] = docs.count
			new_doc["geometry"]["type"] = "MultiPolygon"
			new_doc["geometry"]["coordinates"] = []
			self.find({"properties.neighborhood" => h[:neighborhood]}).each do |n| 
				puts n["geometry"]["coordinates"]
				new_doc["geometry"]["coordinates"].push(n["geometry"]["coordinates"])
			end
			unique_hoods.insert(new_doc)
		end
	end

	def copy_to_final(final_trips)
		puts "[#{__method__}] Beginning copy to 'final_trips'..."
	  bulk = final_trips.initialize_unordered_bulk_op
	  self.find.each { |doc| bulk.insert(doc) }
	  bulk.execute
		puts "[#{__method__}] Copy to 'final_trips' completed."
	end


	def remove_bad_trips
		puts "[#{__method__}] Execution started..."
		query = {"$or" => [
			{"Dropoff_longitude" => {"$type" => { "$ne" => 2 }}},
			{"Dropoff_latitude" => {"$type" => { "$ne" => 2 }}},
			{"Pickup_longitude" => {"$type" => { "$ne" => 2 }}},
			{"Pickup_latitude" => {"$type" => { "$ne" => 2 }}}
			]}
		num = self.find(query).count
		self.find(query).each { |doc| self.remove(doc) } if num > 0		
		puts "[#{__method__}] #{num} records found with bad pickup or dropoff locations. #{num} records removed."
		puts "[#{__method__}] Execution complete."
	end

	def copy_sample(sample_trips, limit, chunk)
		puts "[#{__method__}] Sample collection copy started..."
		sub_query = {"$not" => { "$type" => 2 }}
		match_query = {"$and" => [
			{"Dropoff_longitude" => sub_query },
			{"Dropoff_latitude" => sub_query },
			{"Pickup_longitude" => sub_query },
			{"Pickup_latitude" => sub_query }
			]}
		pipeline = [
				{"$skip" => chunk*limit},
				{"$limit" => limit},
				{"$match" => match_query},
				{"$out" => sample_trips.name}
			]
		self.aggregate(pipeline)
		puts "[#{__method__}] Sample collection copy complete."
	end

	def update_hoods(unique_boroughs, hoods, type)
		puts "[#{__method__}] 'Update hoods' method execution in progress..."	
		running_total = 0
		unique_boroughs.find.each_with_index do |b, bi|
			query_string = { "properties.boroughCode" => b["borough_code"].to_s }
			puts "[#{__method__}] The total neighborhoods found in #{b['borough']} is: #{hoods.find(query_string).count()}."
			trip_count = 0
			bulk = self.initialize_unordered_bulk_op
			last_total = running_total
			hoods.find(query_string).each_with_index do |h, hi|
				trip_query = {
					"#{type}_data.neighborhood" => {"$exists" => false},
					"#{type}_data.borough_code" => b["borough_code"],
					"#{type}_loc" => { "$geoWithin" => {"$geometry" => h["geometry"] } } }
				trip_count = self.find(trip_query).count()
				running_total += trip_count.nil? ? 0 : trip_count
				if trip_count > 0
					bulk.find(trip_query).update({"$set" => {
						"#{type}_data.neighborhood" => h["properties"]["neighborhood"], 
						"#{type}_data.neighborhood_code" => h["properties"]["neighborhood_code"] 
						}})
					puts "[#{__method__}] #{trip_count} trips have #{type}s in #{h['properties']['neighborhood']}. (Running total: #{running_total})"
				end
			end
			bulk.execute if running_total > last_total
		end

		trip_query = { "#{type}_data.borough_code" => {"$exists" => false} }
		update_params = { "$set" => {
			"#{type}_data.neighborhood_code" => 0,
			"#{type}_data.neighborhood" => "No neighborhood.",
			"#{type}_data.borough_code" => 0,
			"#{type}_data.borough" => "No borough."
		}}
		result = self.update(trip_query,update_params, {multi: true})
		running_total += result['nModified'].nil? ? 0 : result['nModified']
		puts "[#{__method__}] #{result['nModified']} trips have no #{type} borough or neighborhood. (Running total: #{running_total})"

		trip_query = { "#{type}_data.neighborhood" => {"$exists" => false} }
		update_params = { "$set" => { 
			"#{type}_data.neighborhood" => "Borough, but no neighborhood.",
			"#{type}_data.neighborhood_code" => 0,
			}}
		result = self.update(trip_query,update_params, {multi: true})
		running_total += result['nModified'].nil? ? 0 : result['nModified']
		puts "[#{__method__}] #{result['nModified']} trips have a #{type} borough, but not a #{type} neighborhood. (Running total: #{running_total})"
	end	

	def generate_summary_stats(results_coll, types)
		types.each do |type|
			query = { "#{type}_data.neighborhood" => { "$exists" => true }}
			self.map_reduce(map(type), reduce, {query: query, out: {reduce: results_coll.name}} )
		end
		puts "[#{__method__}] Summary output generated to the collection '#{results_coll.name}'."
	end

	def perform_fact_check(facts)
		self.map_reduce(fact_check_map, reduce, {out: facts.name} )
		puts "[#{__method__}] Fact check completed."
	end

	def update_dates_and_locations
		puts "[#{__method__}] Updating trip locations..."
		self.map_reduce(map_locations, reduce_locations, {out: self.name})
		project_query = {
			"dropoff_data" => "$value.dropoff_data",
			"pickup_data" => "$value.pickup_data",
			"dropoff_loc" => "$value.dropoff_loc",
			"pickup_loc" => "$value.pickup_loc"
		}
		get_trip_properties.each { |p| project_query[p] = "$_id.#{p}" }
		pipeline = [
			{ "$project" => project_query },
			{ "$out" => self.name }
		]
		self.aggregate(pipeline)
		puts "[#{__method__}] Trip geometry points updated in '#{self.name}'."
	end

	def create_trip_indexes(types)
		types.each do |t|
			puts "[#{__method__}] Creating index on the #{t} location field..."
			self.create_index([["#{t}_loc", Mongo::GEO2DSPHERE]])
			puts "[#{__method__}] Trips index created on the #{t} location field without issue."
			puts "[#{__method__}] Creating index on the #{t} borough_code field..."
			self.create_index({"#{t}_data.borough_code" => Mongo::ASCENDING})
			puts "[#{__method__}] Trips index created on the #{t} borough_code field without issue."
		end
	end

	def create_geo_index
		self.create_index([["geometry", Mongo::GEO2DSPHERE]])
		puts "[#{__method__}] #{self.name} geometry index created without issue..."
	end

	def find_trip_boroughs(boroughs, type)
		puts "[#{__method__}] Checking trips for #{type}s."
		bulk = self.initialize_unordered_bulk_op
		tot = 0
		boroughs.find.each_with_index do |b, i|
			query = {"#{type}_data.borough" => {"$exists" => false}, "#{type}_loc" => { "$geoWithin" => {"$geometry" => b["geometry"] } } }
			trip_count = self.find(query).count
			if trip_count > 0
				tot += trip_count
				bulk.find(query).update({"$set" =>
					{
						"#{type}_data.borough" => b["properties"]["borough"],
						"#{type}_data.borough_code" => b["properties"]["boroughCode"]
					}
				})
				puts "[#{__method__}] #{trip_count} trips have #{type} locations in #{b["properties"]["borough"]}."
			end
		end
		bulk.execute if tot > 0
	end 

	def find_unique_boroughs(unique_boroughs)
		puts "[#{__method__}] Finding unique boroughs..."
		unique_boroughs.remove
		all_boroughs = self.find.map { |b| {borough_code: b["properties"]["boroughCode"], borough: b["properties"]["borough"] } }.uniq!
		all_boroughs.each { |b| unique_boroughs.insert(b) }
		puts "[#{__method__}] Unique boroughs found."
	end

	def generate_summary_coll(keys, coll, type)
		puts "*** starting aggregation ... ***"

		mapper = summary_map(keys) if type == 0
		mapper = trip_map(keys) if type == 1

		self.map_reduce(mapper, reduce, {out: coll.name})
		coll.flatten_mapreduce_output(keys)
		puts "*** completed successfully ***"
	end

	def flatten_mapreduce_output(keys)
		project_query = { "_id" => 0, "type" => "$_id.type", "count" => "$value.count" }
		keys.each { |k| project_query[k] = "$_id.#{k}" }
		pipeline = [
			{"$project" => project_query },
			{"$out" => self.name}
		]
		self.aggregate(pipeline)
	end

	def flatten_summary
		pipeline = [
			{"$project" =>
				{
					"_id" => 0,
					"day_id" => "$_id.data.day_id",
					"day" => "$_id.data.day",
					"date" => "$_id.data.date",
					"year" => "$_id.data.year",
					"week_num" => "$_id.data.week_num",
					"hour" => "$_id.data.hour",
					"min_range" => "$_id.data.min_range",
					"time_range" => "$_id.data.time_range",
					"neighborhood" => "$_id.data.neighborhood",
					"neighborhood_code" => "$_id.data.neighborhood_code",
					"borough_code" => "$_id.data.borough_code",
					"borough" => "$_id.data.borough",
					"type" => "$_id.type",
					"count" => "$value.count"
				}
			},
			{"$out" => self.name}
		]

		self.aggregate(pipeline)
	end
end

def get_trip_properties
	trip_properties = [
		"_id",
		"lpep_pickup_datetime",
		"Lpep_dropoff_datetime",
		"Store_and_fwd_flag",
		"RateCodeID",
		"Pickup_longitude",
		"Pickup_latitude",
		"Dropoff_longitude",
		"Dropoff_latitude",
		"Passenger_count",
		"Trip_distance",
		"Fare_amount",
		"Extra",
		"MTA_tax",
		"Tip_amount",
		"Tolls_amount",
		"Ehail_fee",
		"Total_amount",
		"Payment_type",
		"Distance_between_service",
		"Time_between_service",
		"Trip_type"
	]
	return trip_properties

end