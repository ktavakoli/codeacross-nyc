def time_converter(x)
	sec_in_min = 60
	sec_in_hour = sec_in_min * 60
	sec_in_day = sec_in_hour * 24
	if x < sec_in_min
		x = "#{x.round(1)} seconds"
	elsif x > sec_in_min && x < sec_in_hour
		x = "#{(x*1.0/sec_in_min).round(1)} minutes"
	elsif x > sec_in_hour && x < sec_in_day
		x = "#{(x*1.0/sec_in_min).round(1)} hours"
	elsif x > sec_in_day
		x = "#{(x*1.0/sec_in_min).round(1)} days"
	end
	return x
end

class Mongo::DB
	def initialize_sample_db(sample_db)
		puts "[#{__method__}] #{sample_db.collections.count} collections in the sample database."
		sample_db.command({"dropDatabase" => 1})

		sample_collections = [
			{coll: "nycNeighborhoods", limit: nil},
			{coll: "nycBoroughs", limit: nil}
		]

		sample_collections.each do |s|
			coll = self.collection(s[:coll])
			target_coll = sample_db.collection(s[:coll])
			source_coll = s[:limit].nil? ? coll.find : coll.find.limit(s[:limit])
			bulk = target_coll.initialize_unordered_bulk_op
			source_coll.each { |doc| bulk.insert(doc) }
			bulk.execute
			puts "[#{__method__}] Migrated #{coll.name} successfully..."
		end

		puts "[#{__method__}] Collections successfully migrated from the main database to the sample database."

	end

	def clean_db
		collections = [
			{coll: "taxiTrips", remove_fields: ["dropoff_borough", "start_hood"]},
			{coll: "nycNeighborhoods", remove_fields: ["evaluated", "sample_matches"]},
			{coll: "nycBoroughs", remove_fields: ["evaluated", "sample_matches"]}
		]

		collections.each do |c|
			coll = collection(c[:coll])
			bulk = coll.initialize_unordered_bulk_op
			subtotal = 0
			c[:remove_fields].each do |f|
				query = {f => {"$exists" => true}}
				subtotal = coll.find(query).count()
				if subtotal > 0
					bulk.find(query).update({"$unset" => {f => true }})
				end
				puts "[#{__method__}] #{f} removed from #{c[:coll]}."
			end
			bulk.execute if subtotal > 0
		end

		puts "[#{__method__}] Database sucessfully cleaned up."

	end

end

class Mongo::Collection
	def remove_bad_trips
		puts "[#{__method__}] Execution started..."
		query = {"$or" => [
			{"Dropoff_longitude" => {"$type" => 2 }},
			{"Dropoff_latitude" => {"$type" => 2 }},
			{"Pickup_longitude" => {"$type" => 2 }},
			{"Pickup_latitude" => {"$type" => 2 }}
			]}
		num = self.find(query).count
		self.find(query).each { |doc| self.remove(doc) } if num > 0		
		puts "[#{__method__}] #{num} records found with bad pickup or dropoff locations. #{num} records removed."
		puts "[#{__method__}] Execution complete."
	end

	def copy_sample(target, opts={})
		puts "[#{__method__}] Collection copy started..."
		bulk = target.initialize_unordered_bulk_op
		self.find({},opts).each { |row| bulk.insert(row) }
		bulk.execute
		puts "[#{__method__}] Collection copy complete."
	end


	def update_hoods(unique_boroughs, hoods, type)
		puts "[#{__method__}] 'Update hoods' method execution in progress..."	
		running_total = 0
		unique_boroughs.find.each_with_index do |b, bi|
			query_string = { "properties.boroughCode" => b["borough_code"].to_s }
			puts "[#{__method__}] The total neighborhoods found in #{b['borough']} is: #{hoods.find(query_string).count()}."
			trip_count = 0
			bulk = self.initialize_unordered_bulk_op
			last_total = running_total
			hoods.find(query_string).each_with_index do |h, hi|
				trip_query = {
					"#{type}_data.neighborhood" => {"$exists" => false},
					"#{type}_data.borough_code" => b["borough_code"],
					"#{type}_loc" => { "$geoWithin" => {"$geometry" => h["geometry"] } } }
				trip_count = self.find(trip_query).count()
				running_total += trip_count.nil? ? 0 : trip_count
				if trip_count > 0
					bulk.find(trip_query).update({"$set" => {"#{type}_data.neighborhood" => h["properties"]["neighborhood"] }})
					puts "[#{__method__}] #{trip_count} trips have #{type}s in #{h['properties']['neighborhood']}. (Running total: #{running_total})"
				end
			end
			bulk.execute if running_total > last_total
		end

		trip_query = { "#{type}_data.borough_code" => {"$exists" => false} }
		update_params = { "$set" => {
			"#{type}_data.neighborhood" => "No neighborhood.",
			"#{type}_data.borough_code" => 0,
			"#{type}_data.borough" => "No borough."
		}}
		result = self.update(trip_query,update_params, {multi: true})
		running_total += result['nModified'].nil? ? 0 : result['nModified']
		puts "[#{__method__}] #{result['nModified']} trips have no #{type} borough or neighborhood. (Running total: #{running_total})"

		trip_query = { "#{type}_data.neighborhood" => {"$exists" => false} }
		update_params = { "$set" => { "#{type}_data.neighborhood" => "Borough, but no neighborhood." }}
		result = self.update(trip_query,update_params, {multi: true})
		running_total += result['nModified'].nil? ? 0 : result['nModified']
		puts "[#{__method__}] #{result['nModified']} trips have a #{type} borough, but not a #{type} neighborhood. (Running total: #{running_total})"



	end	

	def generate_summary_stats(results_coll, types)
		types.each do |type|
			query = { "#{type}_data.neighborhood" => { "$exists" => true }}
			self.map_reduce(map(type), reduce, {query: query, out: {reduce: results_coll.name}} )
		end
		puts "[#{__method__}] Summary output generated to the collection '#{results_coll.name}'."
	end

	def perform_fact_check(facts)
		self.map_reduce(fact_check_map, reduce, {out: facts.name} )
		puts "[#{__method__}] Fact check completed."
	end

	def update_dates(type)

		source_date = case type
		when "pickup" then "lpep_pickup_datetime"
		when "dropoff" then "Lpep_dropoff_datetime"
		end

		bulk = self.initialize_unordered_bulk_op

		self.find.each do |doc|
			dt = DateTime.parse("#{doc[source_date]} EST")
			min_range = "#{"%02d" % (dt.min / 15 * 15)}-#{"%02d" % (((dt.min / 15)+1) * 15)}"
			day = dt.strftime("%Y-%m-%d")
			hour = dt.hour
			wday = dt.strftime("%a")

			bulk.find({"_id" => doc["_id"]}).update({"$set" => { 
				"#{type}_data.bucket_weekday" => wday,
				"#{type}_data.bucket_date" => day,
				"#{type}_data.bucket_hour" => hour,
				"#{type}_data.bucket_min" => min_range
				}})

		end
		bulk.execute
		puts "[#{__method__}] Dates updated for all #{type}s."

	end

	def update_locations
		puts "[#{__method__}] Updating trip locations..."
		bulk = self.initialize_unordered_bulk_op
		self.find.each do |t|
			bulk.find({"_id" => t["_id"]}).update({ "$set" => { 
				dropoff_loc: { type: "Point", coordinates: [t["Dropoff_longitude"],  t["Dropoff_latitude"]] },
				pickup_loc: { type: "Point", coordinates: [t["Pickup_longitude"],  t["Pickup_latitude"]] }
				}})
		end
		bulk.execute
		puts "[#{__method__}] Trip geometry points updated in '#{self.name}'."
	end

	def create_trip_loc_indexes(types)
		types.each do |t|
			self.create_index([["#{t}_loc", Mongo::GEO2DSPHERE]])
			puts "[#{__method__}] Trips index created on the #{t} location field without issue..."
		end
	end

	def create_trip_borough_indexes(types)
		types.each do |t|
			self.create_index({"#{t}_data.borough_code" => Mongo::ASCENDING})
			puts "[#{__method__}] Trips index created on the #{t} borough_code field without issue..."
		end
	end

	def create_borough_index
		self.create_index([["geometry", Mongo::GEO2DSPHERE]])
		puts "[#{__method__}] Boroughs index created without issue..."
	end

	def find_trip_boroughs(boroughs, type)
		puts "[#{__method__}] Checking trips for #{type}s."
		bulk = self.initialize_unordered_bulk_op
		boroughs.find.each_with_index do |b, i|
			query = {"#{type}_data" => {"$exists" => false}, "#{type}_loc" => { "$geoWithin" => {"$geometry" => b["geometry"] } } }
			trip_count = self.find(query).count
			if trip_count > 0
				bulk.find(query).update(
					{"$set" => {"#{type}_data" => {"borough" => b["properties"]["borough"],  "borough_code" => b["properties"]["boroughCode"] } } })
				puts "[#{__method__}] #{trip_count} trips have #{type} locations in #{b["properties"]["borough"]}."
			end
		end
		bulk.execute
	end 

	def find_unique_boroughs(unique_boroughs)
		puts "[#{__method__}] Finding unique boroughs..."
		unique_boroughs.remove
		all_boroughs = self.find.map { |b| {borough_code: b["properties"]["boroughCode"], borough: b["properties"]["borough"] } }.uniq!
		all_boroughs.each { |b| unique_boroughs.insert(b) }
		puts "[#{__method__}] Unique boroughss found."
	end
end