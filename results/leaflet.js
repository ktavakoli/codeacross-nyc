var createMap = function() {
	var map = L.map('map').setView([40.791, -73.952], 13);

	L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
			'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="http://mapbox.com">Mapbox</a>',
		id: 'examples.map-i875mjb7'
	}).addTo(map);

	data = $('#map').data('dropoffs');
	dataLen = data.length;

	console.log("The total number of records found appended to the data-dropoffs attribute is: " + dataLen);

	for (var i = 0; i < dataLen; i++) {
		console.log(data[i]);
		L.marker([data[i][1], data[i][0]]).addTo(map);
	}

};

$(document).ready( function () {
	createMap();
});